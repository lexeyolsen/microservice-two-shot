from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.http import JsonResponse
import json
# Create your views here.

class LocationVOEncoder(ModelEncoder):
   model = LocationVO
   properties = ["closet_name"]

class HatEncoder(ModelEncoder):
   model = Hat
   properties = [ "style_name", "fabric", "color", "image", "id"]

   encoders = {
       "location": LocationVOEncoder
   }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
   if request.method == "GET": # GETS LIST OF HATS
       hats = Hat.objects.all()
       print(hats)
       return JsonResponse(
           {"hats": hats} ,
           encoder=HatEncoder,
       )
   else: #CREATES A NEW HAT
       # gets the info from the new post request and converts to python
       content = json.loads(request.body)
       print("CONTENT: ",content)
       try:
           # creating a variable and setting as the href from the content
           location_href = content["location"]
           print("LOCATION HREF::::::::::::::::::",location_href)

           # getting the LocationVO object href that is the same as the one in content
           location = LocationVO.objects.get(id=location_href)
           print("LOCATION:::::::", location)

           # putting the new location in the content
           content["location"] = location
           print("CONTENT:::::::", content["location"])
       except LocationVO.DoesNotExist:
           return JsonResponse(
               {"message:" "Invalid location id"},
               status=400,
               safe=False)
           

       #creating a new hat using the info from content
       hat = Hat.objects.create(**content)
       return JsonResponse(hat, encoder=HatEncoder, safe=False)


@require_http_methods(["GET","DELETE"]) # DELETES A HAT
def api_show_hat(request, pk):
       if request.method == "DELETE":
           count, _ = Hat.objects.filter(id=pk).delete()

           return JsonResponse(
               {"deleted": count >0 }
           )
       else: # SHOWS HAT DETAILS
           hat = Hat.objects.get(id=pk)
           return JsonResponse(
               hat, encoder=HatEncoder, safe=False
           )
