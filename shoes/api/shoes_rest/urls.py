from django.urls import path

from .views import api_delete_shoes, api_list_shoes

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_create_shoes"),
    path("shoes/<int:pk>/", api_delete_shoes, name="api_delete_shoe"),
    
]