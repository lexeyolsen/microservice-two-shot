# Wardrobify

To use this app you will need to start by: 
1. forking this application.
2. cloning this project chosing the https:// version into a folder of your choice using the **git clone https://locationhere** 
3. adding this project to a folder of your choice using git clone /
4. Running the command **docker volume create pgdata**
5. Running the command **docker-compose build**
6. Running the command **docker-compose up**
7. Navigate to **localhost:3000** on your browser and enjoy.

Team:

* Lexey - Which microservice? : Shoes

* Dean - Which microservice? : Hats

## Design

## Shoes microservice

The Shoe model will have the properties "manufacturer", "model_name", "color", "picture_url", and a "bin" foreign key to connect to the BinVO.
The BinVO will have the same properties as the Bin model in wardrobe.
Create the poller to connect the BinVO to the Bin, so that it will update the BinVO when a new entry is added to the Bin.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The models will have key compnents to be able to list out properly what kind of hat is in differing locations. Models will consist of fabric, style name, color, image, and current location. Also by using a Location value object you'll be able to find where your hat is by referencing closet name, section number, and shelf number. The model will be implemented with the use of a poller which will commincate with the database.
