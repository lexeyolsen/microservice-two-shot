function HatsList(props){
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Works</th>
                    <th>Color</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                        </tr>

                    )
                })}

            </tbody>




        </table>
    )
}


export default  HatsList;
