function ShoesList(props) {
    return(
        <table className="table table-striped">
            <thead className="table table-dark">
                <tr>
                    <th>Shoe Name</th>
                    <th>Closet Name</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(s => {
                    return (
                        <tr key={s.id}>
                            <td>{s.model_name}</td>
                            <td>{s.bin.closet_name}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}

export default ShoesList;