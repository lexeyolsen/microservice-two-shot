import React from "react";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './hatslist';
import ShoesList from './ShoesList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatsList hats={props.hats} />} />
          </Route>
          <Route path="">
            <Route path="" element={<ShoesList shoes={props.shoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
